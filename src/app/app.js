import get from 'toolkit/ajax/get';
import data from 'data';
import BaseView from './BaseView';

const app = {
	launch ( el ) {
		app.view = new BaseView({
			el
		});


		//find key
		var urlKey = window.location.search;
		var urlSlice = urlKey.slice(1, urlKey.length);
		var key = urlSlice.split('=')[1];
		
		//find referrer 
		var referrer = document.referrer,
		    referencePreview = referrer.indexOf('gutools.co.uk') > -1;

	
		// figure out URL

			// former version: get( 'http://interactive.guim.co.uk/docsdata-test/' + key +'.json' ).then( function(json) {
			//if need to distinguish btw test and production links, use this: self === top

		get( `https://interactive.guim.co.uk/${ self === top || referencePreview ? 'docsdata-test' : 'docsdata'}/${key}.json` ).then( function(json) {

			var data = JSON.parse( json );
			console.log(data);

			app.view.set({
				timeline: data.sheets.Timeline_data,
				headers: data.sheets.Headers,
				referrer: referencePreview
			});

		}).catch( err => {
			console.log(err);
			app.view.set({ err });
		});
	}


};

window.app = app; // useful for debugging!

export default app;


