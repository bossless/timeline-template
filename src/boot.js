/*global require */
const css = `
	.gia-loader {
		text-align: center;
	}
`;

const html = `
	<div class='gia-loading'>
		<div class="loader">Loading...</div>
	</div>
`;

export function boot ( el ) {
	const body = document.getElementsByTagName( 'body' )[0];
	const head = document.getElementsByTagName( 'head' )[0];

	const style = document.createElement( 'style' );
	const styleSheet = style.styleSheet;
	if ( style.styleSheet ) {
		style.styleSheet.cssText = css;
	} else {
		style.innerHTML = css;
	}

	// NGA fuck-up
	if ( body && body.getAttribute( 'data-content-type' ) === 'article' ) {
		const fontsLink = document.createElement( 'link' );
		fontsLink.setAttribute( 'rel', 'stylesheet' );
		fontsLink.setAttribute( 'href', '<@baseUrl@>/static/fonts.css' );

		head.appendChild( fontsLink );
	}

	head.appendChild( style );

	el.style.margin = '0';
	el.innerHTML = html;

	setTimeout( () => {
		require([ '<@baseUrl@>/<@version@>/js/app.js' ], app => app.launch( el ) );
	});
}
