var gia = exports;

// The project name and unique identifier
gia.name = 'timeline-template';
gia.guid = '3e2aabfe-3168-4b9f-8efb-1b4f4cf08fee';

// The root of the project, relative to http://interactive.guim.co.uk
gia.path = 'embed/2015/07/timeline-template';

// Randomised port, to prevent clashes
gia.port = 6327;

// Project type. Used for deployment - don't change this!
gia.type = 'standalone';
