/*global require */
var path = require( 'path' );
var gobble = require( 'gobble' );
var gia = require( './giafile' );

var deploy = gobble.env() === 'production';
var baseUrl = ( deploy ? ( 'http://interactive.guim.co.uk/' + gia.path ) : '.' );
var version = 'v/' + ( deploy ? Date.now() : 'dev' );

var replacers = {
	baseUrl: baseUrl,
	version: version
};

var build = gobble([

	// static stuff
	gobble( 'static' ).moveTo( 'static' ),

	// index.html
	gobble( 'src/index.html' ),

	// boot.js
	gobble( 'src/boot.js' )
		.transform( 'replace', replacers )
		.transform( 'babel', {
			sourceMap: false,
			inputSourceMap: false
		})
		.transform( 'rollup', {
			entry: 'boot.js',
			dest: 'boot.js',
			format: 'amd'
		}),

	// app
	gobble([
		gobble( 'src/app' ).exclude( 'data/**' )
			.transform( 'ractive', { type: 'es6' })
			.transform( 'replace', replacers )
			.transform( 'babel', {
				inputSourceMap: false
			}),

		gobble( 'src/app/data' )
			.transform( 'spelunk', {
				dest: 'data.js',
				type: 'es6'
			})
	])
		.transform( 'rollup', {
			entry: 'app.js',
			format: 'cjs',
			sourceMap: true,

			// TODO used named imports from toolkit
			resolveExternal: function ( id ) {
				if ( id.slice( 0, 8 ) === 'toolkit/' ) {
					id = path.resolve( __dirname, 'node_modules/gia-toolkit/dist', id.slice( 8 ) ) + '.js';
					return id;
				}

				return false;
			}
		})
		.transform( 'derequire' )
		.transform( 'browserify', {
			entries: [ './app' ],
			dest: 'app.js',
			standalone: 'GIA',
			debug: true
		})
		.moveTo( version + '/js' ),

	// files
	gobble( 'src/files' ).moveTo( path.join( version, 'files' ) )

]);

if ( deploy ) {
	// TODO and htmlmin, and imagemin
	build = build.transform( 'uglifyjs' );
}

module.exports = build;
